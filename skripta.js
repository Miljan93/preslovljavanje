 $(document).ready(function(){

        var latin = ["A","B","V","G","D","Đ","E","Ž","Z","I","J","K","L","LJ","M","N","NJ","O","P","R","S","T","Ć","U","F","H","C","Č","DŽ","Š","a","b","v","g","d","đ","e","ž","z","i","j","k","l","lj","m","n","nj","o","p","r","s","t","ć","u","f","h","c","č","dž","š"];
        var ciril = ["А","Б","В","Г","Д","Ђ","Е","Ж","З","И","Ј","К","Л","Љ","М","Н","Њ","О","П","Р","С","Т","Ћ","У","Ф","Х","Ц","Ч","Џ","Ш","а","б","в","г","д","ђ","е","ж","з","и","ј","к","л","љ","м","н","њ","о","п","р","с","т","ћ","у","ф","х","ц","ч","џ","ш"];
        var str, n, pom;
        var output = "";
        
        $("#obrada").click(function(){
            str = $("#input").val();
            var count=0;
            for(var i=0; i < 10; i++){ 
                if(jQuery.inArray(str.slice(i, i+1), ciril) !== -1){
                    count++;
                }
            }
            if(count>=5){
                $("#output").val(cirtolat(str));
                output="";
            }
            else{
                $("#output").val(lattocir(str));
                output="";
            }
        });

        function lattocir(x){
            var x = str;
            for(var i=0; i < x.length; i++){ 
                pom = x.slice(i, i+1);
                n = latin.indexOf(pom);
                if(n == -1){
                    output += pom;  
                }
                else if (n == 4 && x.slice(i+1, i+2) == "J" || n == 4 && x.slice(i+1, i+2) == "j"){
                    output += "Ђ";
                    i++;
                }
                else if (n == 4 && x.slice(i+1, i+2) == "Z" || n == 4 && x.slice(i+1, i+2) == "z"){
                    output += "Џ";
                    i++;
                }
                else if (n == 4 && x.slice(i+1, i+2) == "Ž" || n == 4 && x.slice(i+1, i+2) == "ž"){
                    output += "Џ";
                    i++;
                }
                else if (n == 15 && x.slice(i+1, i+2) == "J" || n == 15 && x.slice(i+1, i+2) == "j"){
                    output += "Њ";
                    i++;
                }
                else if (n == 12 && x.slice(i+1, i+2) == "J" || n == 12 && x.slice(i+1, i+2) == "j"){
                    output += "Љ";
                    i++;
                }
                else if (n == 34 && x.slice(i+1, i+2) == "j"){
                    output += "ђ";
                    i++;
                }
                else if (n == 34 && x.slice(i+1, i+2) == "z" || n == 34 && x.slice(i+1, i+2) == "ž"){
                    output += "џ";
                    i++;
                }
                else if (n == 42 && x.slice(i+1, i+2) == "j"){
                    output += "љ";
                    i++;
                }
                else if (n == 45 && x.slice(i+1, i+2) == "j"){
                    output += "њ";
                    i++;
                }
                else output += ciril[n];
            }
            return output;
        }

        function cirtolat(x){
            for(var i=0; i < x.length; i++){ 
                pom = x.slice(i, i+1);
                n = ciril.indexOf(pom);
                if(n == -1){
                    output += pom;  
                }
                else output += latin[n];
            }
            return output;
        }

        $('#input')
        .focus(function() { $(this).css("background", "none").css("background-color", "white") })
        .blur(function() { if ($(this)[0].value == '') { $(this).css("background", "url(images/Pozadina.png) center center no-repeat");  $("#output").val(""); } });
    });             